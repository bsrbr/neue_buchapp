from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from django.template import loader
from .models import *

# Create your views here.

def index(request):
    sprachen = Sprache.objects.all()
    context = {"sprachen": sprachen}
    return render(request, "buchapp/index.html", context)

def my_books(request):
    buch = Book.objects.all()
    context = {"buch": buch}
    return render(request, "buchapp/my_books.html", context)

def book_detail(request, book_id):
    buch = get_object_or_404(Book, pk=book_id)
    context = {"buch": buch}     
    return render(request, "buchapp/book_detail.html", context)

def new_book(request):
    if request.method == 'POST':
        titel = request.POST["titel"]
        autor = request.POST["autor"]
        inhalt = request.POST["inhalt"]
        cover = request.POST["cover"]
        prio = request.POST["prio"]
        sprache = sprache.objects.get(pk=request.POST["sprache"])
        Book.objects.create(titel=titel,
                            autor=autor,
                            inhalt=inhalt,
                            cover=cover,
                            prio=prio)
        return HttpResponseRedirect(reverse('buchapp:my_books'))
    sprachen = Sprache.objects.all()
    context = {"sprachen": sprachen}

    return render(request, "buchapp/new_book.html", context)