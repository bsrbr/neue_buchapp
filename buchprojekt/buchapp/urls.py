from django.contrib import admin
from django.urls import path, include
from . import views

app_name=''
urlpatterns = [
    path('', views.index, name='index'),
    path('my_books/', views.my_books, name='my_books'),
    path('book_detail/<int:book_id>/', views.book_detail, name='book_detail'),
    path('new_book/', views.new_book, name='new_book')
]