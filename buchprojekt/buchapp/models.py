from django.db import models

# Create your models here.

class Sprache(models.Model):
    sprache = models.CharField(max_length = 50)
    
    def __str__(self):
        return self.sprache    

class Book(models.Model):
    titel = models.CharField(max_length = 500)
    autor = models.CharField(max_length = 200)
    inhalt = models.TextField(blank=True, default=False)
    sprache = models.ForeignKey(Sprache, on_delete=models.CASCADE)
    cover = models.CharField(max_length = 1000)
    prio = models.BooleanField(default=False)
    
    def __str__(self):
        return f'{self.titel}'
