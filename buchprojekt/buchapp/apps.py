from django.apps import AppConfig


class BuchappConfig(AppConfig):
    name = 'buchapp'
